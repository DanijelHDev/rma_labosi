package com.hrcek.danijel.rma_lab1_calculator.repository;


import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hrcek.danijel.rma_lab1_calculator.R;
import com.hrcek.danijel.rma_lab1_calculator.entity.Calculator;

import java.util.regex.Pattern;

public class CalculatorRepository {
    public Activity activity;

    public CalculatorRepository(Activity _activity){
        this.activity = _activity;
        RegisterComponents();
        SetDefaultCalcValues();
    }

    public Button bttn0;
    public Button bttn1;
    public Button bttn2;
    public Button bttn3;
    public Button bttn4;
    public Button bttn5;
    public Button bttn6;
    public Button bttn7;
    public Button bttn8;
    public Button bttn9;
    public Button bttnDelete;
    public Button bttnResult;
    public Button bttnPlus;
    public Button bttnMultiply;
    public Button bttnDivide;
    public Button bttnMinus;
    public TextView txtInput;
    public TextView txtResult;

    public void readInput(View view){
        String txtToSet;
        switch(view.getId()){
            case R.id.bttnZero:
                txtToSet = bttn0.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnOne:
                txtToSet = bttn1.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnTwo:
                txtToSet = bttn2.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnThree:
                txtToSet = bttn3.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnFour:
                txtToSet = bttn4.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnFive:
                txtToSet = bttn5.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnSix:
                txtToSet = bttn6.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnSeven:
                txtToSet = bttn7.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnEight:
                txtToSet = bttn8.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnNine:
                txtToSet = bttn9.getText().toString();
                checkInputAction(txtToSet);
                break;
            case R.id.bttnDelete:
                SetDefaultCalcValues();
                txtResult.setText("");
                checkInputAction("");
                break;
            case R.id.bttnPlus:
                if(txtInput.getText() == null || txtInput.getText().toString().isEmpty()) break;
                txtToSet = bttnPlus.getText().toString();
                if(!Calculator.getOperandSet())
                    Calculator.setOperandOne(Double.parseDouble(txtInput.getText().toString()));
                else
                    break;
                Calculator.setOperandSet(true);
                Calculator.setOperation(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                checkInputAction(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                txtInput.setText(Calculator.getInputString());
                break;
            case R.id.bttnMinus:
                if(txtInput.getText() == null || txtInput.getText().toString().isEmpty()) break;
                txtToSet = bttnMinus.getText().toString();
                if(!Calculator.getOperandSet())
                    Calculator.setOperandOne(Double.parseDouble(txtInput.getText().toString()));
                else
                    break;
                Calculator.setOperandSet(true);
                Calculator.setOperation(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                checkInputAction(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                txtInput.setText(Calculator.getInputString());
                break;
            case R.id.bttnDivide:
                if(txtInput.getText() == null || txtInput.getText().toString().isEmpty()) break;
                txtToSet = bttnDivide.getText().toString();
                if(!Calculator.getOperandSet())
                    Calculator.setOperandOne(Double.parseDouble(txtInput.getText().toString()));
                else
                    break;
                Calculator.setOperandSet(true);
                Calculator.setOperation(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                checkInputAction(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                txtInput.setText(Calculator.getInputString());
                break;
            case R.id.bttnMultiply:
                if(txtInput.getText() == null || txtInput.getText().toString().isEmpty()) break;
                txtToSet = bttnMultiply.getText().toString();
                if(!Calculator.getOperandSet())
                    Calculator.setOperandOne(Double.parseDouble(txtInput.getText().toString()));
                else
                    break;
                Calculator.setOperandSet(true);
                Calculator.setOperation(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                checkInputAction(txtToSet);
                Calculator.setInputString(Calculator.getInputString() + " ");
                txtInput.setText(Calculator.getInputString());
                break;
            case R.id.bttnResult:
                writeResult();
                break;
        }
    }
    public void checkInputAction(String clicked) {
        //Ako nema nikakvog texta u inputu - Append
        if(txtInput.getText() == null || txtInput.getText().toString().isEmpty() ){
            Calculator.setInputString(clicked);
            writeInput();
        //Ako ima teksta ali operand nije postavljen, append
        }else if(txtInput.getText() != null && !txtInput.getText().toString().isEmpty() && !Calculator.getOperandSet()){
            Calculator.setInputString(Calculator.getInputString() + clicked);
            writeInput();
        //Ako je dodan neki operand
        }else if(txtInput.getText() != null || !txtInput.getText().toString().isEmpty() && Calculator.getOperandSet()){
            Calculator.setInputString(Calculator.getInputString() + clicked);
            writeInput();
        }
    }

    private void writeResult() {
        if(Calculator.getOperation() == null || Calculator.getOperation().isEmpty()) return;
        switch (Calculator.getOperation()){
            case "+":
                try{
                    Calculator.setOperandTwo(Double.parseDouble(txtInput.getText().toString().split(Pattern.quote("+"))[1]));
                }catch (Exception e){
                    Calculator.setOperandTwo(null);
                }

                if(Calculator.getOperandOne() == null || Calculator.getOperandTwo() == null) break;
                Calculator.setResult(Calculator.getOperandOne() + Calculator.getOperandTwo());
                txtResult.setText(Double.toString(Calculator.getResult()));
                SetDefaultCalcValues();
                break;
            case "-":
                try{
                    Calculator.setOperandTwo(Double.parseDouble(txtInput.getText().toString().split(Pattern.quote("-"))[1]));
                }catch (Exception e){
                    Calculator.setOperandTwo(null);
                }
                if(Calculator.getOperandOne() == null || Calculator.getOperandTwo() == null) break;
                Calculator.setResult(Calculator.getOperandOne() - Calculator.getOperandTwo());
                txtResult.setText(Double.toString(Calculator.getResult()));
                SetDefaultCalcValues();
                break;
            case "*":
                try{
                    Calculator.setOperandTwo(Double.parseDouble(txtInput.getText().toString().split(Pattern.quote("*"))[1]));
                }catch (Exception e){
                    Calculator.setOperandTwo(null);
                }
                if(Calculator.getOperandOne() == null || Calculator.getOperandTwo() == null) break;
                Calculator.setResult(Calculator.getOperandOne() * Calculator.getOperandTwo());
                txtResult.setText(Double.toString(Calculator.getResult()));
                SetDefaultCalcValues();
                break;
            case "/":
                try{
                    Calculator.setOperandTwo(Double.parseDouble(txtInput.getText().toString().split(Pattern.quote("/"))[1]));
                }catch (Exception e){
                    Calculator.setOperandTwo(null);
                }
                if(Calculator.getOperandOne() == null || Calculator.getOperandTwo() == null) break;
                try{
                    Calculator.setResult(Calculator.getOperandOne() / Calculator.getOperandTwo());
                    txtResult.setText(Double.toString(Calculator.getResult()));
                    SetDefaultCalcValues();
                }catch(Exception e){
                    txtResult.setText(e.getMessage());
                }
                break;
        }
    }

    public void writeInput(){
        txtInput.setText(Calculator.getInputString());
    }

    private void SetDefaultCalcValues() {
        Calculator.setInputString("");
        Calculator.setOperandSet(false);
        Calculator.setInputString("");
        Calculator.setOperandOne(null);
        Calculator.setOperandTwo(null);
        writeInput();
    }

    public void RegisterComponents(){
        bttn0 = (Button) activity.findViewById(R.id.bttnZero);
        bttn1 = (Button) activity.findViewById(R.id.bttnOne);
        bttn2 = (Button) activity.findViewById(R.id.bttnTwo);
        bttn3 = (Button) activity.findViewById(R.id.bttnThree);
        bttn4 = (Button) activity.findViewById(R.id.bttnFour);
        bttn5 = (Button) activity.findViewById(R.id.bttnFive);
        bttn6 = (Button) activity.findViewById(R.id.bttnSix);
        bttn7 = (Button) activity.findViewById(R.id.bttnSeven);
        bttn8 = (Button) activity.findViewById(R.id.bttnEight);
        bttn9 = (Button) activity.findViewById(R.id.bttnNine);
        bttnDelete = (Button) activity.findViewById(R.id.bttnDelete);
        bttnResult = (Button) activity.findViewById(R.id.bttnResult);
        bttnPlus = (Button) activity.findViewById(R.id.bttnPlus);
        bttnMinus = (Button) activity.findViewById(R.id.bttnMinus);
        bttnMultiply = (Button)activity.findViewById(R.id.bttnMultiply);
        bttnDivide = (Button) activity.findViewById(R.id.bttnDivide);
        txtInput = (TextView) activity.findViewById(R.id.txtViewInput);
        txtResult = (TextView) activity.findViewById(R.id.txtViewResult);
    }
}
