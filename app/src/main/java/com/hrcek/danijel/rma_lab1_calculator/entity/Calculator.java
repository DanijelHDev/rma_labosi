package com.hrcek.danijel.rma_lab1_calculator.entity;

public class Calculator {
    private static Double operandOne;
    private static Double operandTwo;

    private static String operation;
    private static String inputString;
    private static Double result;

    public static String getOperation() {
        return operation;
    }

    public static void setOperation(String operation) {
        Calculator.operation = operation;
    }

    public static Double getResult() {

        return result;
    }

    public static void setResult(Double result) {
        Calculator.result = result;
    }

    private static boolean operandSet;

    public static boolean getOperandSet() { return operandSet; }

    public static void setOperandSet(boolean operandSet) {
        Calculator.operandSet = operandSet;
    }

    public static String getInputString() {
        return inputString;
    }

    public static void setInputString(String inputString) {
        Calculator.inputString = inputString;
    }

    public static Double getOperandOne() {
        return operandOne;
    }

    public static void setOperandOne(Double operandOne) {
        Calculator.operandOne = operandOne;
    }

    public static Double getOperandTwo() {
        return operandTwo;
    }

    public static void setOperandTwo(Double operandTwo) {
        Calculator.operandTwo = operandTwo;
    }

}
